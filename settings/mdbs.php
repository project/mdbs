<?php

$form['mdc'] = [
  '#type' => 'details',
  '#title' => t('MDC'),
  '#group' => 'configuration',
  '#tree' => TRUE,
];

$form['mdc']['drawer'] = [
  '#type' => 'fieldset',
  '#title' => t('Drawer'),
  '#tree' => TRUE,
  '#visible' => TRUE,
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
];
$form['mdc']['drawer']['enabled'] = [
  '#type' => 'checkbox',
  '#title' => t('Enable MDC drawer'),
  '#default_value' => theme_get_setting('mdc.drawer.enabled', $theme),
];
$form['mdc']['drawer']['variant'] = [
  '#type' => 'select',
  '#title' => t('Drawer Variant'),
  '#options' => [
    // 'permanent' => t('Permanent'),
    'dismissible' => t('Dismissible'),
    'modal' => t('Modal'),
  ],
  '#default_value' => theme_get_setting('mdc.drawer.variant', $theme),
];
$form['mdc']['drawer']['side'] = [
  '#type' => 'select',
  '#title' => t('Drawer Side'),
  '#options' => [
    'left' => t('Left'),
    'right' => t('Right'),
  ],
  '#default_value' => theme_get_setting('mdc.drawer.side', $theme),
];


$form['mdc']['button'] = [
  '#type' => 'fieldset',
  '#title' => t('Button'),
  '#tree' => TRUE,
  '#visible' => TRUE,
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
];
$form['mdc']['button']['variant'] = [
  '#type' => 'select',
  '#title' => t('Button Variant'),
  '#options' => [
    'default' => t('Default'),
    'outlined' => t('Outlined'),
    'raised' => t('Raised'),
    'unelevated' => t('Unelevated'),
  ],
  '#default_value' => theme_get_setting('mdc.button.variant', $theme),
];

$form['mdc']['textfield'] = [
  '#type' => 'fieldset',
  '#title' => t('Textfield'),
  '#tree' => TRUE,
  '#visible' => TRUE,
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
];
$form['mdc']['textfield']['variant'] = [
  '#type' => 'select',
  '#title' => t('Textfield Variant'),
  '#options' => [
    'default' => t('Default'),
    'outlined' => t('Outlined'),
  ],
  '#default_value' => theme_get_setting('mdc.textfield.variant', $theme),
];

$form['mdc']['select'] = [
  '#type' => 'fieldset',
  '#title' => t('Select'),
  '#tree' => TRUE,
  '#visible' => TRUE,
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
];
$form['mdc']['select']['variant'] = [
  '#type' => 'select',
  '#title' => t('Select Variant'),
  '#options' => [
    'default' => t('Default'),
    'outlined' => t('Outlined'),
    'enhanced' => t('Enhanced'),
  ],
  '#default_value' => theme_get_setting('mdc.select.variant', $theme),
];

$form['mdc']['password'] = [
  '#type' => 'fieldset',
  '#title' => t('Password'),
  '#tree' => TRUE,
  '#visible' => TRUE,
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
];

$form['mdc']['password']['variant'] = [
  '#type' => 'select',
  '#title' => t('Password Variant'),
  '#options' => [
    'default' => t('Default'),
    'outlined' => t('Outlined'),
  ],
  '#default_value' => theme_get_setting('mdc.password.variant', $theme),
];

$form['mdc']['textarea'] = [
  '#type' => 'fieldset',
  '#title' => t('Textarea'),
  '#tree' => TRUE,
  '#visible' => TRUE,
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
];

$form['mdc']['textarea']['variant'] = [
  '#type' => 'select',
  '#title' => t('Textarea Variant'),
  '#options' => [
    'outlined' => t('Outlined'),
  ],
  '#default_value' => theme_get_setting('mdc.textarea.variant', $theme),
];

$form['mdc']['textarea']['counter'] = [
  '#type' => 'checkbox',
  '#title' => t('Enable Character Counter (attribute "maxlength" must be set)'),

  '#default_value' => theme_get_setting('mdc.textarea.counter', $theme),
  '#attributes' => [
    'data-name' => 'textarea_counter',
  ],
];

$form['mdc']['textarea']['maxlength'] = [
  '#type' => 'number',
  '#title' => t('Maxlength value'),
  '#default_value' => theme_get_setting('mdc.textarea.maxlength', $theme),
  '#min' => 1,
  '#states' => [
    'visible' => [
      'input[data-name="textarea_counter"]' => ['checked' => TRUE],
    ],
    'required' => [
      'input[data-name="textarea_counter"]' => ['checked' => TRUE],
    ],
  ],
];
