<?php

namespace Drupal\mdbs_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * @Block(
 *   id = "mdbs_menu_button",
 *   admin_label = @Translation("MDBS Menu Button"),
 *   category = @Translation("MDBS")
 * )
 */
class MenuButton extends BlockBase {

/**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = array();

    $build['menu_button'] = array(
      '#theme' => 'mdbs_menu_button',
    );

    return $build;
  }

}
