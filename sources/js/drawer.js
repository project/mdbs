var body = document.querySelector('body');

// Instantiate MDC Drawer
var drawerEl = document.querySelector('.mdc-drawer');
var drawer = new mdc.drawer.MDCDrawer.attachTo(drawerEl);

// Instantiate MDC Top App Bar (required)
var topAppBarEl = document.querySelector('.region--header-main--wrapper');
var topAppBar = new mdc.topAppBar.MDCTopAppBar.attachTo(topAppBarEl);

topAppBar.setScrollTarget(document.querySelector('.section--content'));
topAppBar.listen('MDCTopAppBar:nav', function() {
  drawer.open = !drawer.open;
});

// Menu button.
var menu_button = document.querySelector('.mdbs-menu-btn .mdc-top-app-bar__navigation-icon');

drawer.listen('MDCDrawer:opened', function() {
  body.classList.add('mdc-drawer--modal--opened');
  menu_button.innerHTML = 'close';
});
drawer.listen('MDCDrawer:closed', function() {
  body.classList.remove('mdc-drawer--modal--opened');
  menu_button.innerHTML = 'menu';
});
