(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.mdbsFileUpload = {
    attach: function () {
      $(document).on('change', '.file-field input[type="file"]', function (e) {
        var $this = $(e.target);
        var $fileField = $this.closest('.file-field');
        var $pathInput = $fileField.find('.file-path');
        var $pathInputPlaceholder = $pathInput.attr('data-placeholder');
        var files = $this[0].files;
        var fileNames = [];

        if (Array.isArray(files)) {
          files.forEach(function (file) {
            return fileNames.push(file.name);
          });
        } else {
          Object.keys(files).forEach(function (key) {
            fileNames.push(files[key].name);
          });
        }

        var $chosenFiles = fileNames.join(', ');

        if ($chosenFiles.length > 0) {
          $pathInput.html($chosenFiles);
        }
        else {
          $pathInput.html($pathInputPlaceholder);
        }
      });
    }
  };
})(jQuery, Drupal);
