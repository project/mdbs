<?php

/**
 * Setting up helper.
 * @param $variables
 */
function _mdbs_setup__drawer(&$variables) {
  // MDBS settings.
  if (!isset($variables['mdc']['drawer']) && isset($variables['theme']['settings']['mdc']['drawer'])) {
    $variables['mdc']['drawer'] = $variables['theme']['settings']['mdc']['drawer'];
  }
  if (!isset($variables['mdc']['drawer']['enabled'])) {
    $variables['mdc']['drawer']['enabled'] = FALSE;
  }
  if (!isset($variables['mdc']['drawer']['variant'])) {
    $variables['mdc']['drawer']['variant'] = 'modal';
  }
  if (!isset($variables['mdc']['drawer']['side'])) {
    $variables['mdc']['drawer']['side'] = 'left';
  }
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function mdbs_preprocess_region__drawer_main(&$variables) {
  _mdbs_setup__drawer($variables);

  if ($variables['mdc']['drawer']['variant'] == 'dismissible' || $variables['mdc']['drawer']['variant'] == 'modal') {
    $variables['#attached']['library'][] = 'mdbs/mdbs.material';
    $variables['#attached']['library'][] = 'mdbs/mdbs.drawer';
  }
}
