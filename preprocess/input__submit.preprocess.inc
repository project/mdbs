<?php

/**
 * Setting up helper.
 * @param $variables
 */
function _mdbs_setup__button(&$variables) {
  // MDBS settings.
  if (isset($variables['element']['#mdc'])) {
    $variables['mdc'] = $variables['element']['#mdc'];
  }
  if (!isset($variables['mdc']['button']['variant']) && isset($variables['theme']['settings']['mdc']['button']['variant'])) {
    $variables['mdc']['button']['variant'] = $variables['theme']['settings']['mdc']['button']['variant'];
  }
  if (!isset($variables['mdc']['button']['variant'])) {
    $variables['mdc']['button']['variant'] = 'default';
  }
}

/**
 * Adding MDBS specific classes depending on type of the input.
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function mdbs_preprocess_input__submit(&$variables) {
  if (isset($variables['element']['#mdc_skip']) && $variables['element']['#mdc_skip'] === TRUE) {
    $variables['mdc']['skip'] = TRUE;
    return;
  }

  _mdbs_setup__button($variables);

  $variables['#attached']['library'][] = 'mdbs/mdbs.material';
}
