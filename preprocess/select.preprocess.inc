<?php

/**
 * Setting up helper.
 * @param $variables
 */
function _mdbs_setup__select(&$variables) {
  // MDBS settings.
  if (isset($variables['element']['#mdc'])) {
    $variables['mdc'] = $variables['element']['#mdc'];
  }
  if (!isset($variables['mdc']['select']['variant'])) {
    $variables['mdc']['select']['variant'] = $variables['theme']['settings']['mdc']['select']['variant'];
  }
  if (!isset($variables['mdc']['select']['variant'])) {
    $variables['mdc']['select']['variant'] = 'default';
  }
}

/**
 * Adding MDBS specific classes depending on type of the input.
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function mdbs_preprocess_select(&$variables) {
  if (isset($variables['element']['#mdc_skip']) && $variables['element']['#mdc_skip'] === TRUE) {
    $variables['mdc']['skip'] = TRUE;
    return;
  }

  _mdbs_setup__select($variables);

  // @todo remove this commented block after the testing.
  // Variant: default or outlined.
  // $variables['mdc']['select']['variant'] = 'default';
  // $variables['mdc']['select']['variant'] = 'outlined';

  // Label.
  $variables['element']['#title_display'] = isset($variables['element']['#title']) && $variables['element']['#title'] ? TRUE : FALSE;
  $variables['label'] = isset($variables['element']['#title']) && $variables['element']['#title'] && $variables['element']['#title_display'] ? $variables['element']['#title'] : NULL;
  $variables['label_display'] = $variables['element']['#title_display'];
}
