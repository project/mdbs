<?php

/**
 * Adding MDBS specific classes to form elements.
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function mdbs_preprocess_form_element__checkbox(&$variables) {
  if (isset($variables['element']['#mdc_skip']) && $variables['element']['#mdc_skip'] === TRUE) {
    $variables['mdc']['skip'] = TRUE;
    return;
  }

  _mdbs_setup__checkbox($variables);

  $variables['element']['#description_display'] = isset($variables['element']['#description']) && $variables['element']['#description'] ? TRUE : FALSE;
  $variables['description']['content'] = isset($variables['element']['#description']) && $variables['element']['#description'] && $variables['element']['#description_display'] ? $variables['element']['#description'] : NULL;
  $variables['description_display'] = $variables['element']['#description_display'];

  $variables['#attached']['library'][] = 'mdbs/mdbs.material';
}
