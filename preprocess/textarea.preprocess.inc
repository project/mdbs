<?php

/**
 * Setting up helper.
 * @param $variables
 */
function _mdbs_setup__textarea(&$variables) {
  // MDBS settings.
  if (isset($variables['element']['#mdc'])) {
    $variables['mdc'] = $variables['element']['#mdc'];
  }
  if (!isset($variables['mdc']['textarea']['variant']) && isset($variables['theme']['settings']['mdc']['textarea']['variant'])) {
    $variables['mdc']['textarea']['variant'] = $variables['theme']['settings']['mdc']['textarea']['variant'];
  }
  if (!isset($variables['mdc']['textarea']['variant'])) {
    $variables['mdc']['textarea']['variant'] = 'outlined';
  }

  if (!isset($variables['mdc']['textarea']['counter']) && isset($variables['theme']['settings']['mdc']['textarea']['counter']) && _mdbs_webform_counter_exists($variables) === FALSE) {
    $variables['mdc']['textarea']['counter'] = $variables['theme']['settings']['mdc']['textarea']['counter'];
  }
  if (!isset($variables['mdc']['textarea']['counter'])) {
    $variables['mdc']['textarea']['counter'] = FALSE;
  }

  if (!isset($variables['mdc']['textarea']['maxlength']) && isset($variables['theme']['settings']['mdc']['textarea']['maxlength'])) {
    $variables['mdc']['textarea']['maxlength'] = $variables['theme']['settings']['mdc']['textarea']['maxlength'];
  }
  if (!isset($variables['mdc']['textarea']['maxlength'])) {
    $variables['mdc']['textarea']['maxlength'] = FALSE;
  }
}

/**
 * Check if textarea webform counter exists.
 *
 * @param $variables
 *
 * @return bool
 */
function _mdbs_webform_counter_exists($variables) {
  if (isset($variables["element"]["#webform"])) {
    if (isset($variables["element"]["#counter_minimum"]) || isset($variables["element"]["#counter_maximum"])) {
      return TRUE;
    }
    return FALSE;
  }
  return FALSE;
}

/**
 * Adding MDBS specific classes depending on type of the input.
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function mdbs_preprocess_textarea(&$variables) {
  if (isset($variables['element']['#mdc_skip']) && $variables['element']['#mdc_skip'] === TRUE) {
    $variables['mdc']['skip'] = TRUE;
    return;
  }

  _mdbs_setup__textarea($variables);

  // @todo remove this commented block after the testing.
  // Variant: outlined.
  // $variables['mdc']['textarea']['variant'] = 'outlined';
  // Maxlength attribute:
  // $variables['attributes']['maxlength'] = 18;

  // Label.
  $variables['element']['#title_display'] = isset($variables['element']['#title']) && $variables['element']['#title'] ? TRUE : FALSE;
  $variables['label'] = isset($variables['element']['#title']) && $variables['element']['#title'] && $variables['element']['#title_display'] ? $variables['element']['#title'] : NULL;
  $variables['label_display'] = $variables['element']['#title_display'];
}
