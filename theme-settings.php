<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implementation of hook_form_system_theme_settings_alter()
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 *
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function mdbs_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  // Get the theme name we are editing
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  include_once 'settings/mdbs.php';
}
